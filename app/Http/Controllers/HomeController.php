<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller

{
    public function utama()
    {
        return view('welcome');
    }

    public function bio()
    {
        return view('halaman.biodata');
    }

    public function (Request $request)
    {
        $Firstname= $request['nama'];
        $Lastname= $request['nama'];
        $Gender = $request['wn'];
        $Nationality = $request ['Nationality'];
        $Languagespoken = $request ['Skill'];
        $Biodata = $request ['bio'];
        return view('welcome.html' , ['nama' => $Firstname, 'nama' => $Lastname, 'wn' => $Gender,'Nationality' => $Nationality, 'Languagespoken' => $Skill, 'bio' => $Biodata]);
    }

}
